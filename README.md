# ICF Live Church Templates

This template can be used for an ICF Live Stream Page.

Simply use the html from [views/index.html](views/index.html) as a template. This includes the CSS and JS from the icf server.

## Installation
Go to `Theme` » choose your Theme » `HTML` » `Template` and past the content of the [views/index.html](views/index.html)

All colors color settings have no effect and come from this theme.

The following Settings still have effect:

- Bible Tab and Mobile Optimizing 
- Social Share Icons
- Mobile Optimization options


# Development
## Terrific Micro
For information on how to use Terrific Micro, please refer to [terrific-micro.md](project/docs/terrific-micro.md) .

