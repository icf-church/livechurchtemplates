app.on('event:live', function(){
  $('.co-chat-tab').trigger('click');
});

/* Hide and show mobile schedule */
$(function(){
  /*$('[data-behavior=show-schedule]').on('click', function(e){
    e.preventDefault();
    $(this).hide();
    $('[data-behavior=hide-schedule]').show();
    $('[data-behavior=schedule]').slideDown();
    $('.co-tabs-content .co-active').removeClass('co-active');
    $('.co-tabs-content .co-schedule-content').addClass('co-active');
    $('.use-mobile-schedule.hide-for-small').removeClass('hide-for-small');
  });
  $('[data-behavior=hide-schedule]').on('click', function(e){
    e.preventDefault();
    $(this).hide();
    $('[data-behavior=show-schedule]').show();
    $('[data-behavior=schedule]').slideUp();
  });*/
});

window.reflowWidgetTabs = function() {
  var container, index, items, newTabs, tabContainer, tabs, reflowTabs, willOverflow;
  
  // change position of schedule…
  var $schedule = $('[data-behavior=widgets]').detach();
  var $wrapper = $('.widget-wrapper');
  var $mobilewrapper = $('.mobile-widgets-wrapper');
  
  console.log($mobilewrapper.is(":visible"));
  if($mobilewrapper.is(":visible")){
    $mobilewrapper.append( $schedule );
  } else {
    $wrapper.append( $schedule );
  }
/*
  container    = $(".more-widgets");
  tabContainer = $(".co-widget-tabs");
  reflowTabs   = [];
  
  container.find("ul li").detach().appendTo(tabContainer);
  container.hide().find("ul").remove();
  tabContainer.removeClass('has-more-widgets');
  tabContainer.find('li').each(function(){ if(this.offsetTop > 0) { willOverflow = true; } });
  
  if( willOverflow ){ tabContainer.addClass('has-more-widgets'); }

  tabContainer.find('li').each(function(){
    if(this.offsetTop > 0) {
      reflowTabs.push(this);
    }
  });
  if (reflowTabs.length > 0) {
    newTabs = $("<ul>").append(reflowTabs);
    container.show().append(newTabs);
  }*/
};

var lazyReflow = _.debounce(reflowWidgetTabs, 300);
$(window).resize(lazyReflow)
$(function(){ setTimeout(function(){ reflowWidgetTabs(); }, 0); });
app.on('private-chat:add private-chat:remove', function(){ reflowWidgetTabs(); });